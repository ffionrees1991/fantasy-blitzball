trigger current_Signing_Creator on Player__c (after update, after insert) {

    if(currentSigningCreatorHelper.isFirstTime){
        currentSigningCreatorHelper.isFirstTime = false;

    // Collect a List of team_Position__c records which the Player__c records are related to, where the Player__c field is not equal to null. Select id from team_Position__c, and Current_Team_Position_String__c from Player__c. **This is a Child to Parent query. ***Is the second parameter necessary? Would this not be blank??
    // List<team_Position__c> team_Position_List = [SELECT id, name, Player__r.Current_Team_Position_String__c FROM team_Position__c WHERE Player__c != NULL];

    // Try child to parent query
    List<Player__c> players_list = [SELECT id, Name, Current_Team_Position_String__c,(SELECT Id, Name  FROM team_Positions__r) FROM Player__c];
    
    // For loop to iterate through the created list
    For(Player__c p : players_list){
    
    // If statement to determine if the Current Team Position field on Player object is null. // THIS NOT NEEDED, CAN UPDATE ANYWAY?
        //if(tp.Player__r.Current_Team_Position_String__c == null){     
            
            string n = p.Name;
            p.Current_Team_Position_String__c = n;
            // assign Player__c record ID to a string variable, then set this for the lookup field on team_Position__c 
            // string player_id = String.valueOf(tp.Id);
            // system.debug(player_id);
            // tp.Player__r.Current_Team_Signing__c = player_id;        
            system.debug(p.Current_Team_Position_String__c);
            
        }
    
    // RE-QUERY
    // List<team_Position__c> team_Position_List_update = [SELECT id, Player__r.Current_Team_Position_String__c FROM team_Position__c WHERE Player__c != NULL];
        
    // Debug to see if assignment has worked
    
    // Perform update ** it's because the update is only updating the Team Position object. Need to get a list of Player sObjects to update where they have the values set?
    update players_list;
    }

}