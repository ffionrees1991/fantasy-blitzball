trigger playerString on team_Position__c (after update, before insert){

    if(playerStringHelper.isFirstTime){
        playerStringHelper.isFirstTime = false;

        // query team_Position__c for records where Player__c is not null
        List<team_Position__c> teamPostitions = [SELECT id, Player__r.name FROM team_Position__c WHERE Player__c != null];

        // loop through records, assign the Player_String__c field
        for(team_Position__c tp : teamPostitions){
            string p = tp.Player__r.Name;
            tp.Player_String__c = p;
        }
        
        update teamPostitions;
    }

}