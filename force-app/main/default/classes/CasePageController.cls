public with sharing class CasePageController {
    
    // testing Show keyword //
    
    public List<Account> AccList{set;get;}
    public boolean showAccList{set;get;}
    
    
    // run instance of this controller to retrieve accounts. What happens when commented out? The list is never populated.
    
    public CasePageController(){
    this.AccList = [SELECT Name FROM Account];
    }
    
    
   
    public PageReference gotoAccList(){
		
        // what happens when this is commented out? list still populated.
        showAccList = True;
        
        PageReference PageRef= new PageReference('/apex/AccountListV2'); 
        return pageRef;       
	         
    }
    
    

    // declare method
    public List<case> getOpenCases(){
    
    // query open cases
    List<Case> openCases = [SELECT id, CaseNumber, AccountId, Priority, Status FROM Case WHERE Status != 'Closed'];
	
    return openCases;
    }

    

    public void save(){

    }

    public void TestSave(){

    }

    public string comment{set; get;}
}