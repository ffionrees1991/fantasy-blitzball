public with sharing Class ComponentsPageCtrl{

    // public List<Account> AccList = new List<Account>();

    public Boolean showAccounts{set;get;}

    // run instance of this controller to set defaults
    public ComponentsPageCtrl(){

        this.showAccounts = false;

    }

    // method to show component here
    public void showAccounts(){
        
        this.showAccounts = true;

    }

    // method to get accounts
    public List<Account> getAccList(){
        List<Account> AccList = [SELECT id, name FROM Account];
        return AccList;
    }

    // method to hid component
    public void hideAccounts(){

        this.showAccounts = false;
    }

    public ComponentsPageCtrl getController(){return this;}

}