public class Season_Team_Picker_Controller {

    // constructor, runs method to populate select lists
    public Season_Team_Picker_Controller(){
        this.showPlayersList = false;
        this.showPositionsList = true;
        PlayerSelectOptionsList PlayerSelectOptionsList = new List<PlayerSelectOptionsList>();
        

    }
        
    // // declare the variables
    // public List<SelectOption> PositionsList {get;set;}
    public Boolean showPlayersList {get;set;}
    public Boolean showPositionsList {get;set;}
    public List<SelectOption> playerSelOptList {get;set;}
    public String clickedPosition {get;set;}
    public String clickedPositionValue {get;set;}
    public String clickedPlayer {get;set;}
    public String clickedPlayerValue {get;set;}
    public List<PlayerSelectOptions> PlayerSelectOptionsList {get;set;}
    
    // get list of positions
    public list<SelectOption> getPositionsList(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Forward','Forward'));
        options.add(new SelectOption('Midfield','Midfield'));
        options.add(new SelectOption('Defender','Defender'));
        options.add(new SelectOption('Goalkeeper','Goalkeeper'));
        return options;
        }

    // wrapper class
    class PlayerSelectOptions {
        Player__c p {get;set;}
        public list<selectoption> listOfPlayers{set;get;}
        List<Player__c> playerList;
        
        public PlayerSelectOptions(Player__c p){
            listOfPlayers =  new list<selectoption>();
            listOfPlayers.add(new selectoption('None','None'));
            playerList= new List<Player__c>([select id, name, best_position__c from Player__c]);
            for(Player__c pl : playerList){           
                listOfPlayers.add(new selectoption(pl.name,pl.name));
            }
        }        
    }

    // set position chosen by user
    public void passClickedFieldValue(){
        clickedPosition = clickedPositionValue;
    }

    public void passClickedPlayer(){
        clickedPlayer = clickedPlayerValue;
    }
  
       
    // method to show component here
    public void showPlayersList(){
        this.showPlayersList = true;
    }
    
}