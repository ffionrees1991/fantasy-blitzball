@isTest
private class current_Signing_Creator_Trigger_test{

    @isTest static Void test_method() {
    
    // Test data setup
    // Create a Player without a Team Position set

    Player__c test_player = New Player__c(
        name = 'mr. Test',
        Shoot__c = 1, 
        Pass__c = 1, 
        Endurance__c = 1, 
        Attack__c = 1, 
        Block__c = 1, 
        Speed__c = 1, 
        Best_Position__c = 'being a test', 
        Player_Power__c = 1, 
        Game__c = 'FFXII');

    insert test_player;

    //Create a Team
    Team__c test_team = new Team__c(Game__c = 'FFXII');
    insert test_team;

    // Create a Team Position with the Player set to the above. Also, Team__c is a required field.
    team_Position__c test_team_Position = New team_Position__c(Player__c = test_player.Id, Team__c = test_team.Id);
    insert test_team_Position;

    // Perform test

    //Test.startTest();

    // update the player record
    integer attack_update = 1;
    test_player.Attack__c = attack_update;
    update test_player;
    
    //Test.stopTest();

    // verify      
    system.assertEquals(test_team_Position.Id, test_player.Current_Team_Position_String__c);
    }

}