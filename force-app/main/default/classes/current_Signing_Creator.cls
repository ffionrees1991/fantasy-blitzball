public class current_Signing_Creator{

public void update_Current_Signing(){

List<Player__c> player_List = [SELECT id, Current_Team_Position_String__c FROM Player__c WHERE Current_Team_Position_String__c = Null];
system.debug(player_List);

// Assign Ids from player_List to Set to cross-reference later
Set<Id> player_Set = (new Map<Id,SObject>(player_List)).keySet();
system.debug(player_set);

// List of Team Positions. Somehow need to reconcile this with the Player list.
List<team_Position__c> team_Position_List = [SELECT id, Player__r.id FROM team_Position__c WHERE Player__c != NULL];
system.debug(team_Position_List);

    // For loop to go through each team_Position__c
    For(team_Position__c tp : team_Position_List){

        // assign Player__c record's Id to variable (from the lookup that has been set on team_Position_List)
        id player_Id = tp.Player__r.id;
        system.debug(player_Id);

        // Loop through list of Player__c records where the Current_Team_Position_String__c field is null
        For(Player__c p : player_List){

            // if the player_Set contains the current records's Player Id, set this value in the Current_Team_Position_String__c field.
            if(player_Set.contains(p.Id)){ 
                system.debug(p.Current_Team_Position_String__c);
                p.Current_Team_Position_String__c = tp.id;
                system.debug(p.Current_Team_Position_String__c);
                }

            }

        }

    // requery and update here?
    List<Player__c> updated_player_List = [SELECT id, Current_Team_Position_String__c FROM Player__c WHERE Current_Team_Position_String__c != Null];
    system.debug(updated_player_List);

    update updated_player_List;

    }

}