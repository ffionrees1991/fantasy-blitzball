public class draftPlayerPageControllerExtension{
    
    private final Player__c pl;
    
    // The extension constructor initializes the private member
    // variable acct by using the getRecord method from the standard
    // controller.
    public draftPlayerPageControllerExtension(ApexPages.StandardController stdController) {
        this.pl = (Player__c)stdController.getRecord();
    }


    // list collecting objects of the class defined below
    public List<Player__c> getPlayerPwrList(){

        List<Player__c> PlayerPwrList = [SELECT Player_Power__c FROM Player__c where id = : ApexPages.currentPage().getParameters().get('id')];
       
        return PlayerPwrList;
    }

}