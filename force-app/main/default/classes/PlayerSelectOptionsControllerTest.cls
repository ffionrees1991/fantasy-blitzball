public class PlayerSelectOptionsControllerTest {
    
    //Our collection of the class/wrapper objects wrapAccount 
    public List<PlayerSelectOptionsWrapper> WrapperList {get;set;}
    public List<Player__c> playerList {get;set;}
    public List<selectOption> playerSelectOptions {get;set;}
    
	public PlayerSelectOptionsControllerTest() {
        
        WrapperList = new List<PlayerSelectOptionsWrapper>();        
        playerList = [Select id, name FROM Player__c];
        playerSelectOptions = new List<selectOption>();

        List<SelectOption> SOs = new List<SelectOption>();
        For(Player__c p : playerList){
            SOs.add(new SelectOption(p.name,p.name));
        }
        // passing both parameters defined in the wrapper class, as obtained above
        For(Player__c p : playerList){
            WrapperList.add(new PlayerSelectOptionsWrapper(SOs,p));            
        }
        // use this to add to list of selectOption
        system.debug(WrapperList[0].listOfPlayers[0]);

        // access the wrapper class object to populate list of select options
        For(SelectOption SO : WrapperList[0].listOfPlayers){
            playerSelectOptions.add(SO);
        }
        system.debug(playerSelectOptions);
        
    }
    
    // need to get the select options out of the wrapper object somehow. Get a listOfPlayers returned from instantiation of the wrapper class obj. Need to write a method with the class as a return type.
    
    // wrapper class. Contains list of selectOptions
    public class PlayerSelectOptionsWrapper{
        
        public list<SelectOption> listOfPlayers{set;get;}
        public Player__c pl {get;set;}
            
        //This is the contructor method. When we create a new wrapAccount object we pass a 
        //Account that is set to the acc property. We also set the selected value to false
        // need to pass a selectoption set to listOfPlayers property       
        public PlayerSelectOptionsWrapper(List<SelectOption> so, Player__c p){
            listOfPlayers = so;
            pl = p;            
        }
    }  
    
}