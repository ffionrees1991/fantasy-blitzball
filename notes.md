// For Triggers, is it the case that the object setting off the process needs to be the one updated? i.e. a Trigger on Player__c will not update records of team_Position__c?? NO



// 12-03 //

is it the case that

multiple related lookups not set in a particular field, are what sets apart just lookups from parent-child stuff?

test



// 13-03 //

https://dfc-org-production.force.com/forums/?id=9062I000000BnOuQAK

Parent-child subqueries always return an embedded List<SObject> in the parent object, even if there is only one (or even zero) child records. Try iterating over the embedded List.